import { Component, OnInit, Input } from '@angular/core';

import { Product } from '@models/product';
// s04
import { SessionService } from '@services/session.service';

// s04
import { Router } from '@angular/router';

// s04 - archiving
import { ProductService } from '@services/product.service';
import Swal from 'sweetalert2';


@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
    @Input() product!: Product;
    @Input() deleteFromView!: Function;

    isAdmin: boolean = false;
    hasToken: boolean = (localStorage.getItem('token') !== null);

    constructor(
        private sessionService: SessionService,
        private productService: ProductService,
        private router: Router
    ) {
        this.isAdmin = sessionService.getIsAdmin();
    }

    ngOnInit(): void {
    }

    // s04
    order(): void { }

    edit(): void {
      this.router.navigate(['/edit-product/' + this.product.id]);
    }

    archive(product: Product): void {
        Swal.fire({
            title: 'Confirm Action', 
            text: 'Do you really want to archive this product?', 
            icon: 'warning',
            showCancelButton: true
        }).then((result) => {
            if (result.isConfirmed) {
                this.productService.archive(this.product.id!).subscribe((response: Record<string, any>) => {
                    Swal.fire('Archive Successful', 'The product has been successfully archived.', 'success');
                    this.deleteFromView;
                });
            }
        });
      }
}