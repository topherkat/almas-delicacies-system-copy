
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

import { Product } from '@models/product';
import { ProductService } from '@services/product.service';

@Component({
    selector: 'app-edit-product',
    templateUrl: './edit-product.component.html',
    styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
    product: Product = new Product();

    constructor(
        private productService: ProductService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        let productId: number = this.route.snapshot.params['id'];

        productService.getOne(productId).subscribe((response: Object) => {
            this.product = response;
        });
    }

    ngOnInit(): void { }

    onSubmit(): void {
        this.productService.update(this.product).subscribe((response: Record<string, any>) => {
            if (response['result'] === 'updated') {
                Swal.fire({
                    title: 'Update Successful',
                    text: 'The product has been updated successfully.',
                    icon: 'success'
                }).then(() => {
                    this.router.navigate(['/products']);
                });
            }
        });
    }
}