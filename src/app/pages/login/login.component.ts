// import { Component } from '@angular/core';
import { Component, OnInit } from '@angular/core';

// s03 - dependency injection
import { UserService } from '@services/user.service';

// s03- SWAL & router to navigate/redirect
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

// s04 - set details to localstorage
// upon login the user details should be saved in local storage that will serve as his access while the session is ongoing (while he is still accessing the websites feature)
import { SessionService } from '@services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string = '';
  password: string = '';

  constructor(
    // s03 - dependency injection
    private userService: UserService,
    private router: Router,

    // s04 - set details to localStorage
    private sessionService: SessionService

  ) { }

  ngOnInit(): void { }

  // onSubmit(): void {
  //     console.log(this.email);
  //     console.log(this.password);

  //     // login function from userService is being called
  //     this.userService.login(this.email, this.password);
  // }

  // onSubmit(): void {
  //   this.userService.login(this.email, this.password).subscribe((response: Record<string, any>) => {
  //       console.log(response);
  //   });
  // }

  onSubmit() {
    this.userService.login(this.email, this.password).subscribe({
        next: this.successfulLogin.bind(this), 
        error: this.failedLogin.bind(this)
    });
  }

  successfulLogin(response: Record<string, any>) {
      // s04
      this.sessionService.setEmail(response['email']);
      this.sessionService.setIsAdmin(response['isAdmin']);
      this.sessionService.setToken(response['token']);
      // -----------
      
      console.log(response);
      // redirect/navigate to home page
      Swal.fire('Login Successful', 'Successful', 'success');
      this.router.navigate(['']);
  }

  failedLogin(result: Record<string, any>) {
      let data: Record<string, any> = result['error'];

      if (data['result'] === 'incorrect_credentials') {
          Swal.fire('Login Failed', 'You have entered incorrect credentials, please try again.', 'error');
      } else if (data['result'] === 'user_not_found') {
          Swal.fire('Login Failed', 'User does not exist, please try again.', 'error');
      }
  }
}