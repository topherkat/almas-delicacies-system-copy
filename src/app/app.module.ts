import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BannerComponent } from './components/banner/banner.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HighlightsComponent } from './components/highlights/highlights.component';

// s02
import { LoginComponent } from './pages/login/login.component';
// import { Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

// s02 continuation
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { RegisterComponent } from './pages/register/register.component';

//s03
// to perform various operations like GET, POST, PUT, DELETE
import { HttpClientModule } from '@angular/common/http';

import { ProductsComponent } from './pages/products/products.component';
import { ProductComponent } from './components/product/product.component';
import { EditProductComponent } from './pages/edit-product/edit-product.component';


//s02
const appRoutes: Routes = [
  // this is where to setup routes to display components (htmls) 
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  // s04
  { path: 'products', component: ProductsComponent },

  // s04 - edit product
  { path: 'edit-product/:id', component: EditProductComponent },
  
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    NavbarComponent,
    HighlightsComponent,
    LoginComponent,
    HomeComponent,
    NotFoundComponent,
    RegisterComponent,
    ProductsComponent,
    ProductComponent,
    EditProductComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
