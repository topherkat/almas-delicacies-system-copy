import { Injectable } from '@angular/core';
import { User } from '@models/user';

import { HttpClient } from '@angular/common/http';

// includes envirnment.ts
// We will use the defined apiUrl environment variable in our services.
import { environment } from '@environments/environment';


import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl: string = environment.apiUrl + '/users';

  constructor(private http: HttpClient) { 
  }

  // login(email: string, password: string): void {
  //   console.log(email);
  //   console.log(password);
  // }

  // login(email: string, password: string): void {
  //   this.http.post(this.baseUrl + '/login', { email, password });
  // }


  // Observable - to handle asynchronous http request and responses when performing login operation
  // Observable does not change the asynchronous nature of HTTP requests and responses. It provides a powerful and convenient way to work with asynchronous data streams in a more functional and reactive manner, making it easier to handle and react to asynchronous events in your application.
  login(email: string, password: string): Observable<Object> {
    // we would like to use our api with a link localhost:80808/api/users/login
    return this.http.post(this.baseUrl + '/login', { email, password });
  }

  register(user: User): Observable<Object> {
    return this.http.post(this.baseUrl + '/register', user);
  }


  order(courseId: number): void { }
}