export class ProductOrder {
    constructor(
        public id?: number,
        public productId?: number,
        public userId?: number,
        public datetimeEnrolled?: Date
    ) { }
}
