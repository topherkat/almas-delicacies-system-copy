export const environment = {
    production: false,
    //  defines the base URL for the API that the application will communicate with during development
    apiUrl: 'http://localhost:8080/api'
};